package data

import "bootcamp/model"

type ITodoData interface {
	GetAllTodo() map[string]*model.Todo
	Create(todo model.Todo) *model.Todo
}

type TodoData struct {
	Todo map[string]*model.Todo
}

func NewTodoData() ITodoData {
	return &TodoData{Todo: map[string]*model.Todo{
		"Dummy Todo": {
			ID:          1,
			Description: "Dummy Todo",
		},
	}}
}

func (r *TodoData) GetAllTodo() map[string]*model.Todo {
	TodoList := r.Todo
	return TodoList
}

func (r *TodoData) Create(todo model.Todo) *model.Todo {
	r.Todo[todo.Description] = &model.Todo{
		ID:          todo.ID,
		Description: todo.Description,
	}
	return r.Todo[todo.Description]
}
