package data_test

import (
	"bootcamp/data"
	"bootcamp/model"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTodoData_Create(t *testing.T) {
	Todo := model.Todo{
		ID:          1,
		Description: "TestTodo",
	}
	Todo2 := model.Todo{
		ID:          2,
		Description: "TestTodo2",
	}
	d := data.NewTodoData()
	beforeCreateLength := len(d.GetAllTodo())

	d.Create(Todo)
	d.Create(Todo2)
	assert.Equal(t, beforeCreateLength+2, len(d.GetAllTodo()))
}

func TestTodoData_GetAllTodo(t *testing.T) {
	Todo := model.Todo{
		ID:          2,
		Description: "TestTodo",
	}
	Todo2 := model.Todo{
		ID:          3,
		Description: "TestTodo2",
	}
	d := data.NewTodoData()
	d.Create(Todo)
	d.Create(Todo2)

	expectedTodoList := map[string]*model.Todo{
		"Dummy Todo": {
			ID:          1,
			Description: "Dummy Todo",
		},
		"TestTodo": {
			ID:          2,
			Description: "TestTodo",
		},
		"TestTodo2": {
			ID:          3,
			Description: "TestTodo2",
		},
	}

	GetTodoList := d.GetAllTodo()
	assert.Equal(t, expectedTodoList, GetTodoList)
}
