package server

import (
	"bootcamp/data"
	"bootcamp/handler"
	"bootcamp/service"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
)

type Server struct{}

func NewServer() *Server {
	return &Server{}
}

func (*Server) StartServer(port int) error {
	d := data.NewTodoData()
	s := service.NewTodoService(d)
	h := handler.NewHandler(s)

	router := mux.NewRouter()
	router.HandleFunc("/api/v1/todoList", h.MainHTTP).Methods("GET")
	router.HandleFunc("/api/v1/addTodo", h.MainHTTP).Methods("POST")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	handler := c.Handler(router)

	err := http.ListenAndServe(fmt.Sprintf(":%d", port), handler)

	return err
}
