# ToDo List Application

Test
http://ec2-35-175-187-77.compute-1.amazonaws.com:3000

Production
http://ec2-3-80-5-157.compute-1.amazonaws.com:3000

## To Do List

- [x] 1- User interface for ONLY adding ToDo’s
- [x] 2- Back-end service to store a persistent state of ToDo list
- [x] 3- Writing deployment files of your front-end and back-end
- [x] 4- Automating build, test and deployment of your application via Gitlab CI/CD pipeline
- [x] 5- Dockerize both your front-end and back-end application to make them ready for deployment.
- [x] 6- Deploy your application to a AWS ECS cloud provider
- [x] 7- Write deployment configurations(i.e. Dockerfile, AWS configure files etc.) for each project.
-
## Tech Stack For Back-End

<ol>
<li>GoLang
    <ol>
        <li>gorilla/mux and cors (web framework)</li>
        <li>testify (test framework)</li>
        <li>map for in-memory database</li>
    </ol>
</li>

<li>Gitlab (CI-CD)
    .gitlab-ci.yml 
<ol>
    <li>Build</li>
    <li>Test</li>
    <li>Dockerize</li>
    <li>deploy2TestEnv</li>
    <li>acceptance-test(trigger)</li>
    <li>can-i-deploy-pact</li>
    <li>deploy2ProdEnv</li>
    </ol>
</li>

<li>Docker for prod and test env</li>

<li>
Cloud Deployment
<ol>
<li> 2 Amazon Elastic Container Registry (Container Registry) (Front-end / Back-end)  </li>
<li> 2 Amazon Elastic Container Service (2 EC2 for deploy) (Front-end / Back-end)  </li>
</ol>
</li>
</ol>


## Pipeline automation

####Build

The first step is to build the project. If there is an error in the code, it will not be able to build, and we learn this early.
We can get build manually in the code below.

`$ - go build`


Test

My unit tests and my pact test are running at the same time. My pact test looks at the place I publish on the frontend and verifies it.

`$ - go test ./...`

When the unit tests run successfully, it starts the next step.

Dockerize

After starting the docker process using golang/alpine, after making my ECR connection operations, I push my docker to the ecr container

Example
![](images/ecr-backend1.png)
![](images/ecr-backend2.png)

Deploy2TestEnv

The docker that I registered with ECR runs the ecr container that I specified in my task via the ECS service on the EC2 virtual machine.

![](images/TestEnvCluster.png)

Acceptance-Test

I triggered the pipeline I created in the Acceptance repository when I got the required tokens here.

Can I Deploy-Pact

Thanks to pactflow, I'm checking the conract I verified. If a successful result is returned, it means that I can deploy now.

![](images/pact.png)

Deploy2ProdEnv

Here, if the acceptance passes successfully, I need to deploy to the prod. Since I already have a docker, I don't need to docker again. I will use it directly to migrate my latest docker to live environment.
![](images/prodenv.png)


Pipeline Layout

![](images/pipeline-back1.png)

![](images/pipeline-back3.png)


Successfully Completed.

## Development enviroment

Project Setup

Start the project 

`go run .`

it runs **:3000**

To run unit tests

`go test ./...`

APIs

`[POST]` `api/v1/addTodo`

`$ curl -H "Content-type: application/json" -X POST -d '{"description":"dummy"}' localhost:3000/api/v1/addTodo`


`[GET]` `api/v1/getTodolist`

`$ curl -H "Content-type: application/json" localhost:3000/api/v1/todoList`

![](images/return.png)

