package handler_test

import (
	"bootcamp/handler"
	mock "bootcamp/mocks"
	"bootcamp/model"
	"bytes"
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_MainHTTP(t *testing.T) {
	t.Run("get all todo", func(t *testing.T) {
		dataReturn := []*model.Todo{
			{
				ID:          1,
				Description: "FirstTodo",
			},
			{
				ID:          2,
				Description: "SecondTodo",
			},
		}

		s := mock.NewMockITodoService(gomock.NewController(t))
		s.EXPECT().
			GetAll().
			Return(dataReturn).
			Times(1)

		h := handler.NewHandler(s)

		r := httptest.NewRequest(http.MethodGet, "/api/v1/todoList", nil)
		w := httptest.NewRecorder()
		h.MainHTTP(w, r)

		result := "[{\"id\":1,\"description\":\"FirstTodo\"},{\"id\":2,\"description\":\"SecondTodo\"}]"
		assert.Equal(t, w.Body.String(), result)
		assert.Equal(t, "application/json", w.Header().Get("content-type"))

	})
	t.Run("diff method return err", func(t *testing.T) {

		s := mock.NewMockITodoService(gomock.NewController(t))
		h := handler.NewHandler(s)
		r := httptest.NewRequest(http.MethodPut, "/api/v1/todoList", nil)
		w := httptest.NewRecorder()
		h.MainHTTP(w, r)

		assert.Equal(t, w.Result().StatusCode, http.StatusInternalServerError)

	})
	t.Run("post with add todo", func(t *testing.T) {
		dataReturn := model.Todo{
			ID:          1,
			Description: "FirstTodo",
		}

		s := mock.NewMockITodoService(gomock.NewController(t))
		s.EXPECT().
			Create("FirstTodo").
			Return(&dataReturn).
			Times(1)

		h := handler.NewHandler(s)
		body := map[string]string{"description": "FirstTodo"}
		lastBody, _ := json.Marshal(body)
		r := httptest.NewRequest(http.MethodPost, "/api/v1/addTodo", bytes.NewReader(lastBody))
		w := httptest.NewRecorder()
		h.MainHTTP(w, r)

		actual := "{\"id\":1,\"description\":\"FirstTodo\"}"
		assert.Equal(t, w.Body.String(), actual)
		assert.Equal(t, w.Result().StatusCode, http.StatusCreated)
		assert.Equal(t, "application/json", w.Header().Get("content-type"))

	})

}
