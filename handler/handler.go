package handler

import (
	"bootcamp/service"
	"encoding/json"
	"net/http"
	"strings"
)

type IHandler interface {
	MainHTTP(w http.ResponseWriter, r *http.Request)
}

type Handler struct {
	service service.ITodoService
}

func (h *Handler) GetTodoList(w http.ResponseWriter, r *http.Request) {
	todos := h.service.GetAll()

	jsonBytes, err := json.Marshal(todos)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes)
}

type TodoBody struct {
	Description string `json:"description"`
}

func (h *Handler) CreateTodo(w http.ResponseWriter, r *http.Request) {
	TBody := TodoBody{}

	err := json.NewDecoder(r.Body).Decode(&TBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	todo := h.service.Create(TBody.Description)

	jsonBytes, err := json.Marshal(todo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(jsonBytes)
}

func (h *Handler) MainHTTP(w http.ResponseWriter, r *http.Request) {
	url := strings.ReplaceAll(r.URL.Path, "/api/v1", "")

	switch {
	case r.Method == http.MethodGet && url == "/todoList":
		h.GetTodoList(w, r)
		return
	case r.Method == http.MethodPost && url == "/addTodo":
		h.CreateTodo(w, r)
		return
	default:
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

func NewHandler(service service.ITodoService) IHandler {
	return &Handler{service: service}
}
