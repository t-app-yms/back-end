package pact

import (
	"bootcamp/server"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"testing"
)

func TestProvider(t *testing.T) {
	svr := server.NewServer()
	go svr.StartServer(3000)

	pact := dsl.Pact{
		Provider:                 "Backend",
		DisableToolValidityCheck: true,
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		BrokerURL:       "https://ymsagdur.pactflow.io",
		BrokerToken:     "wrojg1YGLD2UtHGtfPsgNw",
		ProviderBaseURL: "http://127.0.0.1:3000",
		ProviderVersion: "1.0.0",
		Tags:            []string{"1.0.0"},
		ConsumerVersionSelectors: []types.ConsumerVersionSelector{
			{
				Consumer: "Frontend",
				Tag:      "1.0.0",
			},
		},
		PublishVerificationResults: true, // publish results of verification to PACT broker
	})

	if err != nil {
		t.Log(err)
	}

}
