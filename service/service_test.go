package service_test

import (
	mock "bootcamp/mocks"
	"bootcamp/model"
	"bootcamp/service"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTodoService_GetAll(t *testing.T) {
	dataReturn := map[string]*model.Todo{
		"FirstTodo": {
			ID:          1,
			Description: "FirstTodo",
		},
		"SecondTodo": {
			ID:          2,
			Description: "SecondTodo",
		},
	}

	data := mock.NewMockITodoData(gomock.NewController(t))
	data.
		EXPECT().
		GetAllTodo().
		Return(dataReturn).
		Times(1)

	expectedTodo := make([]*model.Todo, 0)
	expectedTodo = append(expectedTodo, &model.Todo{
		ID:          1,
		Description: "FirstTodo",
	}, &model.Todo{
		ID:          2,
		Description: "SecondTodo",
	})

	s := service.NewTodoService(data)
	actualTodo := s.GetAll()
	assert.Equal(t, &expectedTodo, &actualTodo)

}

func TestTodoService_Create(t *testing.T) {
	t.Run("Create if no record ", func(t *testing.T) {

		expectedTodo := model.Todo{
			ID:          1,
			Description: "TestTodo",
		}
		data := mock.NewMockITodoData(gomock.NewController(t))

		data.EXPECT().GetAllTodo().Return(nil).Times(1)
		data.EXPECT().Create(expectedTodo).Return(&expectedTodo).Times(1)

		s := service.NewTodoService(data)
		TodoCreated := s.Create("TestTodo")

		assert.Equal(t, &expectedTodo, TodoCreated)
	})
	t.Run("Create return existing one ", func(t *testing.T) {
		returnTodo := map[string]*model.Todo{
			"TestTodo": {
				ID:          1,
				Description: "TestTodo",
			},
		}
		expectedTodo := model.Todo{
			ID:          1,
			Description: "TestTodo",
		}
		data := mock.NewMockITodoData(gomock.NewController(t))
		data.EXPECT().GetAllTodo().Return(returnTodo).Times(1)

		s := service.NewTodoService(data)
		TodoCreated := s.Create("TestTodo")
		assert.Equal(t, &expectedTodo, TodoCreated)
	})

}
