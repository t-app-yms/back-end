package service

import (
	"bootcamp/data"
	"bootcamp/model"
	"sort"
)

type ITodoService interface {
	GetAll() []*model.Todo
	Create(string) *model.Todo
}

type TodoService struct {
	data data.ITodoData
}

func NewTodoService(todoData data.ITodoData) ITodoService {
	return &TodoService{data: todoData}
}

func (service TodoService) GetAll() []*model.Todo {
	todo := service.data.GetAllTodo()
	todoList := make([]*model.Todo, 0)

	for _, t := range todo {
		todoList = append(todoList, t)
	}
	sort.Slice(todoList, func(i, j int) bool {
		return todoList[i].ID < todoList[j].ID
	})

	return todoList
}

func (service TodoService) Create(description string) *model.Todo {
	todoMap := service.data.GetAllTodo()
	todoByDescription, exist := todoMap[description]
	if exist == true {
		return todoByDescription
	}

	todoId := len(todoMap) + 1

	todo := model.Todo{
		ID:          todoId,
		Description: description,
	}

	return service.data.Create(todo)
}
