package config

import (
	"encoding/json"
	"io"
	"os"
)

type Config struct {
	AppUrl string `json:"appUrl"`
}

var C = &Config{}

func openConfig(path string) {
	file, err := os.Open(path + env + ".json")
	defer file.Close()
	read, err := io.ReadAll(file)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(read, C)
	if err != nil {
		panic(err)
	}
}
